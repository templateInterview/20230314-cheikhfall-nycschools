package com.fall.nycschools.repositories

import com.fall.nycschools.utils.NetworkStateResponse
import retrofit2.Response
import javax.inject.Inject

/**
 * This base repository is our safe api call helper. This is helpful to catch exceptions
 */
open class BaseRepository @Inject constructor() {

    companion object {
        private const val LOG_TAG = "BaseRepository"
    }

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): NetworkStateResponse<T> {
        return try {
            val response = call.invoke()

            if (response.isSuccessful) {
                response.body()?.let {
                    NetworkStateResponse.Success(it)
                } ?: NetworkStateResponse.Success(null)
            } else {
                val error = if (response.errorBody() != null) {
                    String(response.errorBody()?.bytes()!!)
                } else {
                    "Error occurred during safe Api Call"
                }
                NetworkStateResponse.Error(Exception(error))
            }
        } catch (error: Exception) {
            NetworkStateResponse.Error(error)
        }
    }
}
