package com.fall.nycschools.repositories

import com.fall.nycschools.data.api.SchoolApi
import com.fall.nycschools.data.models.SatScores
import com.fall.nycschools.data.models.School
import com.fall.nycschools.utils.NetworkStateResponse
import com.fall.nycschools.utils.SchoolApiResponse
import javax.inject.Inject

class SchoolRepository @Inject constructor(
    private val schoolApi: SchoolApi
) : BaseRepository() {

    /**
     * Get the list of school
     * @return List<School>
     */
    suspend fun getSchools() : SchoolApiResponse<List<School>> {
        return when (
            val response = safeApiCall(
                call = { schoolApi.getSchool() }
            )
        ) {
            is NetworkStateResponse.Success -> {
                if (!response.data.isNullOrEmpty()) {
                    val schools = response.data.sortedBy { it.schoolName }
                    SchoolApiResponse.Success(schools)
                } else {
                    SchoolApiResponse.Error(Exception("Schools not available"))
                }
            }
            is NetworkStateResponse.Error -> {
                SchoolApiResponse.Error(response.exception)
            }
        }
    }

    /**
     * This method is getting all the available scores, filter through them and return the
     * specific SAT score that is selected
     */
    suspend fun getScores(dbn: String) : SchoolApiResponse<SatScores> {
        return when (
            val response = safeApiCall(
                call = { schoolApi.getScores() }
            )
        ) {
            is NetworkStateResponse.Success -> {
                val score = response.data?.find { it.dbn == dbn }
                if (score != null) {
                    SchoolApiResponse.Success(score)
                } else {
                    SchoolApiResponse.Error(Exception("Scores not available"))
                }
            }
            is NetworkStateResponse.Error -> {
                SchoolApiResponse.Error(response.exception)
            }
        }
    }
}
