package com.fall.nycschools.viewModels

import com.fall.nycschools.data.models.SatScores
import com.fall.nycschools.data.models.School
import com.fall.nycschools.dispatcher.DispatcherProvider
import com.fall.nycschools.repositories.SchoolRepository
import com.fall.nycschools.utils.SchoolApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * This is our viewmodel and a sample unit test is also implemented.
 */
@HiltViewModel
class SchoolViewModel @Inject constructor(
    dispatcherProvider: DispatcherProvider,
    private val schoolRepository: SchoolRepository
) : BaseViewModel(dispatcherProvider) {

    // We are using MutableStateFlow to easily bind the data with our compose ui
    private val _schoolUiState = MutableStateFlow<SchoolUiState>(SchoolUiState.Loading)
    val schoolUiState = _schoolUiState.asStateFlow()

    private val _scoreUiState = MutableStateFlow<ScoreUiState>(ScoreUiState.Loading)
    val scoreUiState = _scoreUiState.asStateFlow()

    var listSchools = listOf<School>()
        private set

    var selectedSchool: School? = null
        private set

    var satScores: SatScores? = null
        private set

    fun getSchools() = scope.launch {
        _schoolUiState.value = SchoolUiState.Loading
        when (val response = schoolRepository.getSchools()) {
            is SchoolApiResponse.Success -> {
                _schoolUiState.value = SchoolUiState.Success(response.data)
            }
            is SchoolApiResponse.Error -> {
                _schoolUiState.value = SchoolUiState.Error(response.exception)
            }
        }
    }

    fun getScore(dbn: String) = scope.launch {
        _scoreUiState.value = ScoreUiState.Loading
        when (val response = schoolRepository.getScores(dbn = dbn)) {
            is SchoolApiResponse.Success -> {
                _scoreUiState.value = ScoreUiState.Success(response.data)
            }
            is SchoolApiResponse.Error -> {
                _scoreUiState.value = ScoreUiState.Error(response.exception)
            }
        }
    }

    fun saveSelectedSchool(school: School) {
        selectedSchool = school
    }

    fun saveSchoolResponse(schools: List<School>) {
        listSchools = schools
    }

    fun saveSatScores(scores: SatScores) {
        this.satScores = scores
    }
}

sealed class SchoolUiState {
    object Loading : SchoolUiState()
    data class Success(val data: List<School>) : SchoolUiState()
    data class Error(val exception: Exception) : SchoolUiState()
}

sealed class ScoreUiState {
    object Loading : ScoreUiState()
    data class Success(val data: SatScores) : ScoreUiState()
    data class Error(val exception: Exception) : ScoreUiState()
}
