package com.fall.nycschools.utils

object Constants {

    // Base url
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"

    // End Points
    const val SCHOOL_END_POINT = "s3k6-pzi2.json"
    const val SCORES_END_POINT = "f9bf-2cp4.json"

    // Style
    const val ROUNDED_PERCENT = 50
}