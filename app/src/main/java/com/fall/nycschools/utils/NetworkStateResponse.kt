package com.fall.nycschools.utils

sealed class NetworkStateResponse<out T : Any> {
    data class Success<out T : Any>(val data: T?) : NetworkStateResponse<T>()
    data class Error(val exception: Exception) : NetworkStateResponse<Nothing>()
}

sealed class SchoolApiResponse<out T : Any> {
    data class Success<out T : Any>(val data: T) : SchoolApiResponse<T>()
    data class Error(val exception: Exception) : SchoolApiResponse<Nothing>()
}
