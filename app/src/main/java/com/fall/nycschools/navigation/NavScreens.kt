package com.fall.nycschools.navigation

/**
 * This sealed class hold all the screen routes to navigate
 */
internal sealed class NavScreens(val route: String) {
    object HomeScreen : NavScreens("home")
    object ListSchoolsScreen : NavScreens("list_schools")
    object LoadListSchoolsScreen : NavScreens("load_list_schools")
    object SchoolDetailsScreen : NavScreens("school_details")
    object LoadSchoolDetailsScreen : NavScreens("load_school_details")
}
