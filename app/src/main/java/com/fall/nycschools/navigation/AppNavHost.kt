package com.fall.nycschools.navigation

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.fall.nycschools.ui.screens.home.HomeScreen
import com.fall.nycschools.ui.screens.school.ListSchoolsScreen
import com.fall.nycschools.ui.screens.school.listSchools.LoadListSchoolsScreen
import com.fall.nycschools.ui.screens.school.schoolDetails.LoadSchoolDetailsScreen
import com.fall.nycschools.ui.screens.school.schoolDetails.SchoolDetailsScreen
import com.fall.nycschools.viewModels.SchoolViewModel

@Composable
fun AppNavHost(viewModel: SchoolViewModel = hiltViewModel()) {
    val navController: NavHostController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = NavScreens.HomeScreen.route
    ) {
        composable(route = NavScreens.HomeScreen.route) {
            HomeScreen(navController = navController)
        }
        composable(route = NavScreens.LoadListSchoolsScreen.route) {
            LoadListSchoolsScreen(navController = navController, viewModel = viewModel)
        }
        composable(route = NavScreens.ListSchoolsScreen.route) {
            ListSchoolsScreen(navController = navController, viewModel = viewModel)
        }
        composable(route = NavScreens.LoadSchoolDetailsScreen.route) {
            LoadSchoolDetailsScreen(navController = navController, viewModel = viewModel)
        }
        composable(route = NavScreens.SchoolDetailsScreen.route) {
            SchoolDetailsScreen(navController = navController, viewModel = viewModel)
        }
    }
}
