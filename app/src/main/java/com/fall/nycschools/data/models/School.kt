package com.fall.nycschools.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class School (
	@SerializedName("dbn") val dbn : String,
	@SerializedName("school_name") val schoolName : String,
	@SerializedName("overview_paragraph") val overviewParagraph : String,
	@SerializedName("phone_number") val phoneNumber : String,
	@SerializedName("school_email") val schoolEmail : String,
	@SerializedName("website") val website : String,
	@SerializedName("total_students") val totalStudents : Int,
	@SerializedName("primary_address_line_1") val primaryAddressLine1 : String,
	@SerializedName("city") val city : String,
	@SerializedName("zip") val zip : String,
	@SerializedName("state_code") val stateCode : String,
) : Parcelable