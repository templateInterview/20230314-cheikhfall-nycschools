package com.fall.nycschools.data.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SchoolResponse(
    val schools: List<School>
) : Parcelable
