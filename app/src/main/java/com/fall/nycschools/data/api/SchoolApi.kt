package com.fall.nycschools.data.api

import com.fall.nycschools.data.models.SatScores
import com.fall.nycschools.data.models.School
import com.fall.nycschools.utils.Constants
import retrofit2.Response
import retrofit2.http.GET

interface SchoolApi {

    @GET(Constants.SCHOOL_END_POINT)
    suspend fun getSchool(): Response<List<School>>

    @GET(Constants.SCORES_END_POINT)
    suspend fun getScores(): Response<List<SatScores>>
}
