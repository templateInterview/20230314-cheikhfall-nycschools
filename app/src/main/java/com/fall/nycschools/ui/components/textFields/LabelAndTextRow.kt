package com.fall.nycschools.ui.components.textFields

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
fun LabelAndTextRow(
    modifier: Modifier = Modifier,
    label: String,
    text: String,
    labelStyle: TextStyle = MaterialTheme.typography.body1,
    textStyle: TextStyle = MaterialTheme.typography.body1,
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .padding(vertical = 10.dp),
    ) {
        DefaultTextView(
            text = "$label: ",
            style = labelStyle
        )
        DefaultTextView(
            text = text,
            style = textStyle
        )
    }
}

@Composable
private fun DefaultTextView(
    text: String,
    modifier: Modifier = Modifier,
    style: TextStyle
) {
    Text(
        text = text,
        style = style,
        modifier = modifier,
        textAlign = TextAlign.Start
    )
}
