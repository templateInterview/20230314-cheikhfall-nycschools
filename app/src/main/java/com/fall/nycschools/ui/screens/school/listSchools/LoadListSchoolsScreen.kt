package com.fall.nycschools.ui.screens.school.listSchools

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.fall.nycschools.R
import com.fall.nycschools.navigation.NavScreens
import com.fall.nycschools.ui.screens.error.FullScreenError
import com.fall.nycschools.ui.screens.loader.FullScreenLoadingIndicator
import com.fall.nycschools.viewModels.SchoolUiState
import com.fall.nycschools.viewModels.SchoolViewModel

@Composable
fun LoadListSchoolsScreen(
    navController: NavHostController,
    viewModel: SchoolViewModel = hiltViewModel()
) {

    LaunchedEffect(Unit) {
        viewModel.getSchools()
    }

    when (val schoolUiState = viewModel.schoolUiState.collectAsState().value) {
        SchoolUiState.Loading -> {
            FullScreenLoadingIndicator()
        }
        is SchoolUiState.Success -> {
            LaunchedEffect(Unit) {
                viewModel.saveSchoolResponse(schools = schoolUiState.data)
                navController.navigateUp()
                navController.navigate(NavScreens.ListSchoolsScreen.route) {
                    launchSingleTop = true
                }
            }
        }
        is SchoolUiState.Error -> {
            FullScreenError(
                errorMessage = stringResource(R.string.general_error_message),
                buttonText = stringResource(R.string.close)
            ) {
                navController.navigateUp()
            }
        }
    }
}
