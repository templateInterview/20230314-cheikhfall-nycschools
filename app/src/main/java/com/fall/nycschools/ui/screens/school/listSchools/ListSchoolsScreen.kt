package com.fall.nycschools.ui.screens.school

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.fall.nycschools.data.models.School
import com.fall.nycschools.navigation.NavScreens
import com.fall.nycschools.ui.components.rows.SchoolRow
import com.fall.nycschools.ui.components.textFields.SearchOutlineTextField
import com.fall.nycschools.utils.searchSchool
import com.fall.nycschools.viewModels.SchoolViewModel

private val listSchools = mutableStateOf(listOf<School>())

@Composable
fun ListSchoolsScreen(
    viewModel: SchoolViewModel,
    navController: NavHostController
) {

    var searchText by remember {
        mutableStateOf("")
    }
    listSchools.value = if (searchText.isBlank()) {
        viewModel.listSchools
    } else {
        viewModel.listSchools.searchSchool(searchText)
    }

    Column(modifier = Modifier.fillMaxSize()) {
        SearchOutlineTextField(
            modifier = Modifier.padding(horizontal = 10.dp, vertical = 10.dp),
            onClearClick = { searchText = "" },
            onSearchTextChanged = { searchText = it }
        )
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            contentPadding = PaddingValues(20.dp)
        ) {
            itemsIndexed(listSchools.value) { index, school ->
                SchoolRow(
                    school = school,
                    number = "${index + 1}",
                    onRowClicked = {
                        viewModel.saveSelectedSchool(school = school)
                        navController.navigate(NavScreens.LoadSchoolDetailsScreen.route) {
                            launchSingleTop = true
                        }
                    }
                )
            }
        }
    }
}