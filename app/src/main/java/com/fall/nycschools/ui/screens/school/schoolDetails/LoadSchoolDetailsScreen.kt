package com.fall.nycschools.ui.screens.school.schoolDetails

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import com.fall.nycschools.R
import com.fall.nycschools.navigation.NavScreens
import com.fall.nycschools.ui.screens.error.FullScreenError
import com.fall.nycschools.ui.screens.loader.FullScreenLoadingIndicator
import com.fall.nycschools.viewModels.SchoolViewModel
import com.fall.nycschools.viewModels.ScoreUiState

@Composable
fun LoadSchoolDetailsScreen(
    navController: NavHostController,
    viewModel: SchoolViewModel
) {

    val school = viewModel.selectedSchool

    LaunchedEffect(Unit) {
        school?.dbn?.let { viewModel.getScore(dbn = it) }
    }

    when (val scoreUiState = viewModel.scoreUiState.collectAsState().value) {
        ScoreUiState.Loading -> {
            FullScreenLoadingIndicator()
        }
        is ScoreUiState.Success -> {
            LaunchedEffect(Unit) {
                viewModel.saveSatScores(scores = scoreUiState.data)
                navController.navigateUp()
                navController.navigate(NavScreens.SchoolDetailsScreen.route) {
                    launchSingleTop = true
                }
            }
        }
        is ScoreUiState.Error -> {
            FullScreenError(
                errorMessage = stringResource(R.string.general_error_message),
                buttonText = stringResource(R.string.close)
            ) {
                navController.navigateUp()
            }
        }
    }
}
