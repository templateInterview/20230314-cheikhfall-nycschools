package com.fall.nycschools.ui.components.rows

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Brightness1
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.fall.nycschools.data.models.School

/**
 * This is a reusable row
 */
@Composable
fun SchoolRow(
    school: School,
    number: String,
    onRowClicked: () -> Unit
) {

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 10.dp)
            .clickable { onRowClicked.invoke() },
        shape = RoundedCornerShape(10.dp),
        elevation = 4.dp
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(10.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            LeadingIcon(number = number)
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = school.schoolName,
                    style = MaterialTheme.typography.subtitle1.copy(
                        fontWeight = FontWeight.Bold
                    ),
                    overflow = TextOverflow.Clip
                )
                Text(
                    text = "${school.city} ${school.stateCode}",
                    style = MaterialTheme.typography.body1,
                    overflow = TextOverflow.Clip
                )
            }
        }
    }
}

@Composable
private fun LeadingIcon(number: String) {
    Box(contentAlignment = Alignment.Center) {
        Icon(
            modifier = Modifier
                .padding(10.dp)
                .size(45.dp),
            imageVector = Icons.Filled.Brightness1,
            tint = Color.Blue,
            contentDescription = null,
        )
        Text(
            modifier = Modifier.offset(0.dp),
            text = number,
            style = MaterialTheme.typography.subtitle1,
            color = Color.White,
            overflow = TextOverflow.Clip,
            maxLines = 1
        )
    }
}