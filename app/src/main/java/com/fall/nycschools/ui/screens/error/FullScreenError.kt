package com.fall.nycschools.ui.screens.error

import android.content.res.Configuration
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.fall.nycschools.R
import com.fall.nycschools.ui.components.buttons.PrimaryButton

private const val SCROLLABLE_SCREEN_WEIGHT = 8.5f

@Composable
fun FullScreenError(
    errorMessage: String,
    buttonText: String,
    onButtonClicked: () -> Unit
) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(SCROLLABLE_SCREEN_WEIGHT),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.error_icon),
                contentDescription = errorMessage,
                modifier = Modifier
                    .padding(5.dp)
                    .size(50.dp)
                    .clip(CircleShape) // clip to the circle shape
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                text = errorMessage,
                modifier = Modifier.padding(4.dp),
                style = MaterialTheme.typography.body1,
                textAlign = TextAlign.Center,
                color = Color.Black,
            )
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min),
            contentAlignment = Alignment.Center
        ) {
            PrimaryButton(
                text = buttonText,
                onClick = { onButtonClicked.invoke() }
            )
        }
        Spacer(modifier = Modifier.height(10.dp))
    }
}

@Preview(
    name = "Night Mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
@Preview(
    name = "Day Mode",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Composable
fun PreviewFullScreenLoaderComponent() {
    FullScreenError(errorMessage = "Something went wrong.", buttonText = "Close", onButtonClicked = {})
}
