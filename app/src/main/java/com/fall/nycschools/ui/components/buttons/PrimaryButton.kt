package com.fall.nycschools.ui.components.buttons

import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.fall.nycschools.utils.Constants

private val PrimaryButtonColors
    @Composable get() = ButtonDefaults.buttonColors(
        backgroundColor = Color.Blue,
        contentColor = Color.White,
    )

/**
 * A reusable button
 * @param onClick Will be called when the user clicks the button
 * @param text String
 */
@Composable
fun PrimaryButton(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    minHeight: Dp = 50.dp,
    minWidth: Dp = 200.dp
) {

    Button(
        modifier = modifier
            .defaultMinSize(minHeight = minHeight, minWidth = minWidth),
        shape = RoundedCornerShape(Constants.ROUNDED_PERCENT),
        onClick = {
            onClick.invoke()
        },
        colors = PrimaryButtonColors
    ) {
        Text(
            text = text,
            color = Color.White,
            style = MaterialTheme.typography.h6
        )
    }
}
