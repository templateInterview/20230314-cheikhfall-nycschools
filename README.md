This project is entirely implemented using Jet Pack Compose. It mainly contains three different screens:

1- The Home screen
![Home screen](screenshots/home_screen.png)
2- The List School School with a search functionality 
![Home screen](screenshots/list_school_screen.png)
3- A School Details Screen that will display the information of a selected school.
![Home screen](screenshots/school_details_screen1.png)

![Home screen](screenshots/school_details_screen2.png)
